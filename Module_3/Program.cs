﻿using System;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }
    }

    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (!int.TryParse(source, out int a))
            {
                throw new Exception();
            }
            return a;
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            if (num1 < 0 && num2 < 0)
            {
                for (int i = 0; i < -num1; i++)
                {
                    result += -num2;
                }
            }
            else if (num1 > 0 && num2 > 0)
            {
                for (int i = 0; i < num1; i++)
                {
                    result += num2;
                }
            }
            else
            {
                for (int i = 0; i < Math.Max(num1, num2); i++)
                {
                    result += Math.Min(num1, num2);
                }
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool parsed = int.TryParse(input, out result) && result > 0;
            if (!parsed)
            {
                throw new Exception();
            }
            return parsed;
        }

        public List<int> GetEvenNumbers(int naturalNumber) //  сделать
        {
            if (naturalNumber <= 0)
            {
                return null;
            }
            List<int> listEvenNumbers = new List<int>(naturalNumber);

            int count = 0;
            for (int i = 0; i < listEvenNumbers.Count; i++)
            {
                if (i % 2 == 0)
                {
                    count++;
                }
                if (count == 0)
                {
                    break;
                }
            }
            return listEvenNumbers;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool parsed = int.TryParse(input, out result) && result > 0;
            if (!parsed)
            {
                throw new Exception("Повторите попытку ввода значения!!!");
            }
            return parsed;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            int res = 0;
            int pow = 1;
            while (source != 0)
            {
                var temp = source % 10;
                source /= 10;
                if (temp == digitToRemove) continue;

                res += temp * pow;
                pow *= 10;
            }
            return Convert.ToString(res);
        }
    }
}